webpackJsonp([1],[
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	const eve_1 = __webpack_require__(1);
	const context_1 = __webpack_require__(2);
	const TodoApp_1 = __webpack_require__(3);
	const count_1 = __webpack_require__(5);
	const filter_1 = __webpack_require__(6);
	const clearCompleted_1 = __webpack_require__(7);
	const local_1 = __webpack_require__(8);
	(function (window) {
	    'use strict';
	    // list name
	    const list = new eve_1.Value().contextName("list name");
	    // the list of todos
	    const todos = new context_1.ToDos().contextName("todos");
	    // the filter name
	    const filterName = new eve_1.Value().contextName("filter name");
	    // render the Web user interface
	    TodoApp_1.todoApp(document.getElementsByClassName("todoapp")[0], todos, filterName);
	    // count the completed and open todos
	    count_1.counting(todos.all, todos.completedCount, todos.openCount);
	    // filter the list
	    filter_1.filter(todos.all, todos.list, filterName);
	    // allow to clear all completed todos
	    clearCompleted_1.clearCompleted(todos.all, todos.clearCompleted);
	    // load a todo list and save changes
	    local_1.local(list, todos.all);
	    // set the list name and a filter to start the app
	    window.onload = () => {
	        filterName.emitSet(document.location.hash.substring(2));
	        list.emitSet(document.location.search.substr(1) || "todos-eve");
	    };
	    window.onhashchange = () => {
	        filterName.emitSet(document.location.hash.substring(2));
	    };
	})(window);


/***/ },
/* 1 */,
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	const eve_1 = __webpack_require__(1);
	class ToDos extends eve_1.Context {
	    constructor() {
	        super(...arguments);
	        this.list = new eve_1.List(ToDo);
	        this.all = new eve_1.List(ToDo);
	        this.openCount = new eve_1.NumberValue(0);
	        this.completedCount = new eve_1.NumberValue(0);
	        this.clearCompleted = new eve_1.Command();
	    }
	}
	exports.ToDos = ToDos;
	class ToDo extends eve_1.Context {
	    constructor() {
	        super(...arguments);
	        this.text = new eve_1.Value();
	        this.completed = new eve_1.BooleanValue(false);
	    }
	}
	exports.ToDo = ToDo;


/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	const eve_1 = __webpack_require__(1);
	const TodoItem_1 = __webpack_require__(4);
	const { E, pick, each, keyCode } = eve_1.dom;
	function todoApp(container, todos, filter) {
	    let chkAll;
	    const panels = [
	        // the header panel
	        E.header({ class: 'header' }, E.h1('todos'), E.input({ class: 'new-todo', placeholder: 'What needs to be done?', autofocus: true }, { onkeyup: onInputKey }), E.header),
	        // the main panel
	        E.section(
	        // one of many ways to dynamically show/hide an element
	        { class: { main: true, hidden: eve_1.match(todos.all.count, 0) } }, chkAll = E.input({ class: 'toggle-all', type: 'checkbox' }, { title: 'Mark all as complete', checked: eve_1.match(todos.openCount, 0) }), E.ul({ class: 'todo-list' }, each(todos.list, TodoItem_1.todoItem)), E.section),
	        // the footer panel
	        E.footer({ class: 'footer' }, 
	        // another way to dynamically show/hide an element
	        { class: pick(todos.all.count, { 0: 'hidden' }) }, E.span({ class: 'todo-count' }, E.strong(todos.openCount), ' item', pick(todos.openCount, { 1: '' }, 's'), ' left'), E.ul({ class: 'filters' }, [['', 'All'], ['active', 'Active'], ['completed', 'Completed']].map((v) => E.li(E.a({ href: '#/' + v[0], class: { selected: eve_1.match(filter, v[0]) } }, v[1]))), E.ul), 
	        // yet another way to dynamically show/hide (or better add/remove from DOM)
	        pick(todos.completedCount, { 0: '' }, E.button({ class: 'clear-completed', onclick: todos.clearCompleted }, 'Clear completed')), E.footer)
	    ];
	    // handle user events
	    function onInputKey(evn) {
	        if (evn.keyCode === keyCode.ENTER) {
	            // add new todo
	            const txt = this.value.trim();
	            if (txt)
	                todos.all.emitAdd(todos.all.createItem()).text.emitSet(txt);
	            this.value = '';
	        }
	    }
	    // complete all
	    // this could also be extracted as a Command (see allInOne.ts)
	    chkAll.onclick = () => {
	        const v = chkAll.checked;
	        todos.all.each((val) => val.completed.emitSet(v));
	    };
	    // insert panels into container
	    for (let panel of panels)
	        container.appendChild(panel);
	}
	exports.todoApp = todoApp;


/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	const eve_1 = __webpack_require__(1);
	const { E, keyCode } = eve_1.dom;
	function todoItem(todo) {
	    // a list item
	    let edit;
	    const el = E.li({ class: { completed: todo.completed } }, E.div({ class: 'view' }, E.input({ class: 'toggle', type: 'checkbox', checked: todo.completed }, { onchange: todo.completed }), E.label(todo.text, { ondblclick: onEdit }), E.button({ class: 'destroy', onclick: () => todo.emitDelete() }), E.div), edit = E.input({ class: 'edit', value: todo.text, onblur: onAccept, onkeydown: onKey }), E.li);
	    // handle user events
	    function onEdit() {
	        el.classList.add('editing');
	        edit.focus();
	    }
	    function onAccept() {
	        el.classList.remove('editing');
	        const txt = edit.value.trim();
	        if (!txt)
	            todo.emitDelete();
	        else
	            todo.text.emitSet(txt);
	    }
	    function onKey(evn) {
	        if (evn.keyCode === keyCode.ENTER)
	            edit.blur();
	        else if (evn.keyCode === keyCode.ESCAPE) {
	            edit.value = todo.text.val();
	            edit.blur();
	        }
	    }
	    return el;
	}
	exports.todoItem = todoItem;


/***/ },
/* 5 */
/***/ function(module, exports) {

	"use strict";
	// completed and open todos
	function counting(todos, completedCount, openCount) {
	    // using connectors...
	    //count(todos, { completed: true }, completedCount)
	    //count(todos, { completed: false }, openCount)
	    // or with different connectors...
	    //count(todos, { completed: true }, completedCount)
	    // TODO: implement
	    //minus(completedCount, todos.count, openCount)
	    // or directly counting both
	    const countFn = () => {
	        let completed = 0, open = 0;
	        todos.each((todo) => todo.completed.val() ? completed++ : open++);
	        completedCount.emitSet(completed);
	        openCount.emitSet(open);
	    };
	    todos.onAdd((todo) => todo.completed.onSet(countFn, todos));
	    todos.onRemove(countFn);
	}
	exports.counting = counting;


/***/ },
/* 6 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	const eve_1 = __webpack_require__(1);
	// filter the list of todos
	function filter(all, filtered, filter) {
	    eve_1.filter(all, { completed: eve_1.map(filter, { completed: true, active: false, '': void 0 }) }, filtered);
	}
	exports.filter = filter;


/***/ },
/* 7 */
/***/ function(module, exports) {

	"use strict";
	// allow to clear all completed todos
	function clearCompleted(all, cmd) {
	    cmd.onPerform(() => {
	        all.each((todo) => todo.completed.val() && todo.emitDelete());
	    });
	}
	exports.clearCompleted = clearCompleted;


/***/ },
/* 8 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	const eve_1 = __webpack_require__(1);
	const { each, idMap } = eve_1.json;
	// store in local storage
	function local(listName, todos) {
	    const tmpl = {
	        todos: each(todos, (todo) => ({
	            id: idMap(todo),
	            text: todo.text,
	            completed: todo.completed
	        }))
	    };
	    const store = new eve_1.BrowserDocStore(localStorage);
	    // load todos as soon as the list name is set
	    listName.onSet((val) => {
	        store.load(val).then((d) => {
	            let idGen = d && d.idGen || 0;
	            const ids = new eve_1.json.IdContextMap(() => idGen++);
	            eve_1.json.build(tmpl, ids, (data) => {
	                data.idGen = idGen;
	                store.save(val, data);
	            });
	            eve_1.json.emit(tmpl, d, ids);
	        });
	    });
	}
	exports.local = local;


/***/ }
]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi90cy9hcHAudHMiLCJ3ZWJwYWNrOi8vLy4vdHMvY29udGV4dC50cyIsIndlYnBhY2s6Ly8vLi90cy91aS9Ub2RvQXBwLnRzIiwid2VicGFjazovLy8uL3RzL3VpL1RvZG9JdGVtLnRzIiwid2VicGFjazovLy8uL3RzL2xvZ2ljL2NvdW50LnRzIiwid2VicGFjazovLy8uL3RzL2xvZ2ljL2ZpbHRlci50cyIsIndlYnBhY2s6Ly8vLi90cy9sb2dpYy9jbGVhckNvbXBsZXRlZC50cyIsIndlYnBhY2s6Ly8vLi90cy9zdG9yYWdlL2xvY2FsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQ0Esc0NBQTJCO0FBQzNCLDBDQUFpQztBQUNqQywwQ0FBc0M7QUFDdEMsd0NBQXdDO0FBQ3hDLHlDQUF1QztBQUN2QyxpREFBdUQ7QUFDdkQsd0NBQXVDO0FBRXZDLEVBQUMsVUFBUyxNQUFNO0tBQ2YsWUFBWSxDQUFDO0tBRWIsWUFBWTtLQUNaLE1BQU0sSUFBSSxHQUFHLElBQUksV0FBSyxFQUFVLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQztLQUN6RCxvQkFBb0I7S0FDcEIsTUFBTSxLQUFLLEdBQUcsSUFBSSxlQUFLLEVBQUUsQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDO0tBQzlDLGtCQUFrQjtLQUNsQixNQUFNLFVBQVUsR0FBRyxJQUFJLFdBQUssRUFBVSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUM7S0FFakUsZ0NBQWdDO0tBQ2hDLGlCQUFPLENBQUMsUUFBUSxDQUFDLHNCQUFzQixDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLEtBQUssRUFBRSxVQUFVLENBQUM7S0FDekUscUNBQXFDO0tBQ3JDLGdCQUFRLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxLQUFLLENBQUMsY0FBYyxFQUFFLEtBQUssQ0FBQyxTQUFTLENBQUM7S0FDMUQsa0JBQWtCO0tBQ2xCLGVBQU0sQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUFFLEtBQUssQ0FBQyxJQUFJLEVBQUUsVUFBVSxDQUFDO0tBQ3pDLHFDQUFxQztLQUNyQywrQkFBYyxDQUFDLEtBQUssQ0FBQyxHQUFHLEVBQUUsS0FBSyxDQUFDLGNBQWMsQ0FBQztLQUMvQyxvQ0FBb0M7S0FDcEMsYUFBSyxDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsR0FBRyxDQUFDO0tBQ3RCLGtEQUFrRDtLQUNsRCxNQUFNLENBQUMsTUFBTSxHQUFHO1NBQ2YsVUFBVSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDdkQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksV0FBVyxDQUFDO0tBQ2hFLENBQUM7S0FDRCxNQUFNLENBQUMsWUFBWSxHQUFHO1NBQ3JCLFVBQVUsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDO0tBQ3hELENBQUM7QUFFRixFQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQzs7Ozs7Ozs7O0FDckNYLHNDQUE0RTtBQUU1RSxZQUFtQixTQUFRLGFBQU87S0FBbEM7O1NBQ0MsU0FBSSxHQUFHLElBQUksVUFBSSxDQUFDLElBQUksQ0FBQztTQUNyQixRQUFHLEdBQUcsSUFBSSxVQUFJLENBQUMsSUFBSSxDQUFDO1NBQ3BCLGNBQVMsR0FBRyxJQUFJLGlCQUFXLENBQUMsQ0FBQyxDQUFDO1NBQzlCLG1CQUFjLEdBQUcsSUFBSSxpQkFBVyxDQUFDLENBQUMsQ0FBQztTQUNuQyxtQkFBYyxHQUFHLElBQUksYUFBTyxFQUFFO0tBQy9CLENBQUM7RUFBQTtBQU5ELHVCQU1DO0FBRUQsV0FBa0IsU0FBUSxhQUFPO0tBQWpDOztTQUNDLFNBQUksR0FBRyxJQUFJLFdBQUssRUFBVTtTQUMxQixjQUFTLEdBQUcsSUFBSSxrQkFBWSxDQUFDLEtBQUssQ0FBQztLQUNwQyxDQUFDO0VBQUE7QUFIRCxxQkFHQzs7Ozs7Ozs7QUNiRCxzQ0FBK0M7QUFFL0MsMkNBQXFDO0FBRXJDLE9BQU0sRUFBQyxDQUFDLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUMsR0FBRyxTQUFHO0FBRXBDLGtCQUF3QixTQUFrQixFQUFFLEtBQVksRUFBRSxNQUE2QjtLQUV0RixJQUFJLE1BQXdCO0tBQzVCLE1BQU0sTUFBTSxHQUFHO1NBQ2QsbUJBQW1CO1NBQ25CLENBQUMsQ0FBQyxNQUFNLENBQUMsRUFBRSxLQUFLLEVBQUUsUUFBUSxFQUFFLEVBQzNCLENBQUMsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLEVBQ2IsQ0FBQyxDQUFDLEtBQUssQ0FBQyxFQUFFLEtBQUssRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUFFLHdCQUF3QixFQUFFLFNBQVMsRUFBRSxJQUFJLEVBQUUsRUFDcEYsRUFBRSxPQUFPLEVBQUUsVUFBVSxFQUFFLENBQUMsRUFDekIsQ0FBQyxDQUFDLE1BQU0sQ0FBQztTQUNWLGlCQUFpQjtTQUNqQixDQUFDLENBQUMsT0FBTztTQUNSLHVEQUF1RDtTQUN2RCxFQUFFLEtBQUssRUFBRSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLFdBQUssQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsRUFBRSxFQUFFLEVBQzVELE1BQU0sR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLEVBQUUsS0FBSyxFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUUsVUFBVSxFQUFFLEVBQ3pELEVBQUUsS0FBSyxFQUFFLHNCQUFzQixFQUFFLE9BQU8sRUFBRSxXQUFLLENBQUMsS0FBSyxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQ3ZFLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxLQUFLLEVBQUUsV0FBVyxFQUFFLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsbUJBQVEsQ0FBQyxDQUFDLEVBQ3hELENBQUMsQ0FBQyxPQUFPLENBQUM7U0FDWCxtQkFBbUI7U0FDbkIsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxFQUFFLEtBQUssRUFBRSxRQUFRLEVBQUU7U0FDM0Isa0RBQWtEO1NBQ2xELEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsRUFBRSxRQUFRLEVBQUUsQ0FBQyxFQUFFLEVBQ2pELENBQUMsQ0FBQyxJQUFJLENBQUMsRUFBRSxLQUFLLEVBQUUsWUFBWSxFQUFFLEVBQzdCLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxFQUFFLE9BQU8sRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsRUFBRSxFQUFFLENBQUMsRUFBRSxFQUFFLEVBQUUsRUFBRSxHQUFHLENBQUMsRUFBRSxPQUFPLENBQUMsRUFDcEYsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEtBQUssRUFBRSxTQUFTLEVBQUUsRUFDeEIsQ0FBQyxDQUFDLEVBQUUsRUFBRSxLQUFLLENBQUMsRUFBRSxDQUFDLFFBQVEsRUFBRSxRQUFRLENBQUMsRUFBRSxDQUFDLFdBQVcsRUFBRSxXQUFXLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsS0FDckUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsSUFBSSxFQUFFLElBQUksR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsS0FBSyxFQUFFLEVBQUUsUUFBUSxFQUFFLFdBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxFQUN2RSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQ1QsQ0FBQyxDQUFDLEVBQUUsQ0FBQztTQUNOLDJFQUEyRTtTQUMzRSxJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsRUFBRSxFQUFFLENBQUMsRUFBRSxFQUFFLEVBQUUsRUFDbkMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxFQUFFLEtBQUssRUFBRSxpQkFBaUIsRUFBRSxPQUFPLEVBQUUsS0FBSyxDQUFDLGNBQWMsRUFBRSxFQUNuRSxpQkFBaUIsQ0FBQyxDQUFDLEVBQ3JCLENBQUMsQ0FBQyxNQUFNLENBQUM7TUFDVjtLQUVELHFCQUFxQjtLQUNyQixvQkFBb0IsR0FBRztTQUN0QixFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsT0FBTyxLQUFLLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO2FBQ25DLGVBQWU7YUFDZixNQUFNLEdBQUcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRTthQUM3QixFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUM7aUJBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxVQUFVLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDO2FBQ3BFLElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRTtTQUNoQixDQUFDO0tBQ0YsQ0FBQztLQUNELGVBQWU7S0FDZiw4REFBOEQ7S0FDOUQsTUFBTSxDQUFDLE9BQU8sR0FBRztTQUNoQixNQUFNLENBQUMsR0FBRyxNQUFNLENBQUMsT0FBTztTQUN4QixLQUFLLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsS0FBSyxHQUFHLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztLQUNsRCxDQUFDO0tBRUQsK0JBQStCO0tBQy9CLEdBQUcsQ0FBQyxDQUFDLElBQUksS0FBSyxJQUFJLE1BQU0sQ0FBQztTQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDO0FBQ3ZELEVBQUM7QUF0REQsMkJBc0RDOzs7Ozs7OztBQzVERCxzQ0FBeUI7QUFHekIsT0FBTSxFQUFDLENBQUMsRUFBRSxPQUFPLEVBQUMsR0FBRyxTQUFHO0FBRXhCLG1CQUF5QixJQUFVO0tBRWxDLGNBQWM7S0FDZCxJQUFJLElBQUk7S0FDUixNQUFNLEVBQUUsR0FBRyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsS0FBSyxFQUFFLEVBQUUsU0FBUyxFQUFFLElBQUksQ0FBQyxTQUFTLEVBQUUsRUFBRSxFQUN2RCxDQUFDLENBQUMsR0FBRyxDQUFDLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxFQUN0QixDQUFDLENBQUMsS0FBSyxDQUFDLEVBQUUsS0FBSyxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUUsVUFBVSxFQUFFLE9BQU8sRUFBRSxJQUFJLENBQUMsU0FBUyxFQUFFLEVBQ3JFLEVBQUUsUUFBUSxFQUFFLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQyxFQUM5QixDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsRUFBRSxVQUFVLEVBQUUsTUFBTSxFQUFFLENBQUMsRUFDMUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxFQUFFLEtBQUssRUFBRSxTQUFTLEVBQUUsT0FBTyxFQUFFLE1BQU0sSUFBSSxDQUFDLFVBQVUsRUFBRSxFQUFFLENBQUMsRUFDaEUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxFQUNQLElBQUksR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLElBQUksRUFBRSxNQUFNLEVBQUUsUUFBUSxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUN2RixDQUFDLENBQUMsRUFBRSxDQUFDO0tBRU4scUJBQXFCO0tBQ3JCO1NBQ0MsRUFBRSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDO1NBQzNCLElBQUksQ0FBQyxLQUFLLEVBQUU7S0FDYixDQUFDO0tBQ0Q7U0FDQyxFQUFFLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUM7U0FDOUIsTUFBTSxHQUFHLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUU7U0FDN0IsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUM7YUFBQyxJQUFJLENBQUMsVUFBVSxFQUFFO1NBQzNCLElBQUk7YUFBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUM7S0FDNUIsQ0FBQztLQUNELGVBQWUsR0FBRztTQUNqQixFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsT0FBTyxLQUFLLE9BQU8sQ0FBQyxLQUFLLENBQUM7YUFBQyxJQUFJLENBQUMsSUFBSSxFQUFFO1NBQzlDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsT0FBTyxLQUFLLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO2FBQ3pDLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUU7YUFDNUIsSUFBSSxDQUFDLElBQUksRUFBRTtTQUNaLENBQUM7S0FDRixDQUFDO0tBRUQsTUFBTSxDQUFDLEVBQUU7QUFDVixFQUFDO0FBbENELDZCQWtDQzs7Ozs7Ozs7QUNwQ0QsNEJBQTJCO0FBQzNCLG1CQUF5QixLQUFpQixFQUN6QyxjQUEyQixFQUFFLFNBQXNCO0tBRW5ELHNCQUFzQjtLQUN0QixtREFBbUQ7S0FDbkQsK0NBQStDO0tBRS9DLGtDQUFrQztLQUNsQyxtREFBbUQ7S0FDbkQsa0JBQWtCO0tBQ2xCLCtDQUErQztLQUUvQyw0QkFBNEI7S0FDNUIsTUFBTSxPQUFPLEdBQUc7U0FDZixJQUFJLFNBQVMsR0FBRyxDQUFDLEVBQUUsSUFBSSxHQUFHLENBQUM7U0FDM0IsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksS0FBSyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsRUFBRSxHQUFHLFNBQVMsRUFBRSxHQUFHLElBQUksRUFBRSxDQUFDO1NBQ2pFLGNBQWMsQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDO1NBQ2pDLFNBQVMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDO0tBQ3hCLENBQUM7S0FDRCxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBVSxLQUFLLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsQ0FBQztLQUNqRSxLQUFLLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQztBQUN4QixFQUFDO0FBckJELDZCQXFCQzs7Ozs7Ozs7QUN6QkQsc0NBQXNEO0FBR3RELDRCQUEyQjtBQUMzQixpQkFBdUIsR0FBZSxFQUFFLFFBQW9CLEVBQUUsTUFBcUI7S0FDbEYsWUFBSSxDQUFDLEdBQUcsRUFBRSxFQUFFLFNBQVMsRUFBRSxTQUFHLENBQUMsTUFBTSxFQUFFLEVBQUUsU0FBUyxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxLQUFLLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRSxRQUFRLENBQUM7QUFDaEcsRUFBQztBQUZELHlCQUVDOzs7Ozs7OztBQ0hELHNDQUFxQztBQUNyQyx5QkFBK0IsR0FBZSxFQUFFLEdBQVk7S0FDM0QsR0FBRyxDQUFDLFNBQVMsQ0FBQztTQUNiLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLEtBQUssSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLEVBQUUsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7S0FDOUQsQ0FBQyxDQUFDO0FBQ0gsRUFBQztBQUpELHlDQUlDOzs7Ozs7OztBQ1JELHNDQUFzRDtBQUd0RCxPQUFNLEVBQUMsSUFBSSxFQUFFLEtBQUssRUFBQyxHQUFHLFVBQUk7QUFFMUIsMEJBQXlCO0FBQ3pCLGdCQUFzQixRQUF1QixFQUFFLEtBQWlCO0tBRS9ELE1BQU0sSUFBSSxHQUFHO1NBQ1osS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQyxJQUFJLEtBQUssQ0FBQzthQUM3QixFQUFFLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQzthQUNmLElBQUksRUFBRSxJQUFJLENBQUMsSUFBSTthQUNmLFNBQVMsRUFBRSxJQUFJLENBQUMsU0FBUztVQUN6QixDQUFDLENBQUM7TUFDSDtLQUVELE1BQU0sS0FBSyxHQUFHLElBQUkscUJBQWUsQ0FBQyxZQUFZLENBQUM7S0FFL0MsNkNBQTZDO0tBQzdDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxHQUFHO1NBQ2xCLEtBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQzthQUN0QixJQUFJLEtBQUssR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLEtBQUssSUFBSSxDQUFDO2FBQzdCLE1BQU0sR0FBRyxHQUFHLElBQUksVUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLEtBQUssRUFBRSxDQUFDO2FBQ2hELFVBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLEdBQUcsRUFBRSxDQUFDLElBQUk7aUJBQzFCLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSztpQkFDbEIsS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDO2FBQ3RCLENBQUMsQ0FBQzthQUNGLFVBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUMsRUFBRSxHQUFHLENBQUM7U0FDeEIsQ0FBQyxDQUFDO0tBQ0gsQ0FBQyxDQUFDO0FBRUgsRUFBQztBQXpCRCx1QkF5QkMiLCJmaWxlIjoiYXBwLmpzIiwic291cmNlc0NvbnRlbnQiOlsiXHJcbmltcG9ydCB7IFZhbHVlIH0gZnJvbSAnZXZlJ1xyXG5pbXBvcnQgeyBUb0RvcyB9IGZyb20gJy4vY29udGV4dCdcclxuaW1wb3J0IHsgdG9kb0FwcCB9IGZyb20gJy4vdWkvVG9kb0FwcCdcclxuaW1wb3J0IHsgY291bnRpbmcgfSBmcm9tICcuL2xvZ2ljL2NvdW50J1xyXG5pbXBvcnQgeyBmaWx0ZXIgfSBmcm9tICcuL2xvZ2ljL2ZpbHRlcidcclxuaW1wb3J0IHsgY2xlYXJDb21wbGV0ZWQgfSBmcm9tICcuL2xvZ2ljL2NsZWFyQ29tcGxldGVkJ1xyXG5pbXBvcnQgeyBsb2NhbCB9IGZyb20gJy4vc3RvcmFnZS9sb2NhbCdcclxuXHJcbihmdW5jdGlvbih3aW5kb3cpIHtcclxuXHQndXNlIHN0cmljdCc7XHJcblxyXG5cdC8vIGxpc3QgbmFtZVxyXG5cdGNvbnN0IGxpc3QgPSBuZXcgVmFsdWU8c3RyaW5nPigpLmNvbnRleHROYW1lKFwibGlzdCBuYW1lXCIpXHJcblx0Ly8gdGhlIGxpc3Qgb2YgdG9kb3NcclxuXHRjb25zdCB0b2RvcyA9IG5ldyBUb0RvcygpLmNvbnRleHROYW1lKFwidG9kb3NcIilcclxuXHQvLyB0aGUgZmlsdGVyIG5hbWVcclxuXHRjb25zdCBmaWx0ZXJOYW1lID0gbmV3IFZhbHVlPHN0cmluZz4oKS5jb250ZXh0TmFtZShcImZpbHRlciBuYW1lXCIpXHJcblxyXG5cdC8vIHJlbmRlciB0aGUgV2ViIHVzZXIgaW50ZXJmYWNlXHJcblx0dG9kb0FwcChkb2N1bWVudC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKFwidG9kb2FwcFwiKVswXSwgdG9kb3MsIGZpbHRlck5hbWUpXHJcblx0Ly8gY291bnQgdGhlIGNvbXBsZXRlZCBhbmQgb3BlbiB0b2Rvc1xyXG5cdGNvdW50aW5nKHRvZG9zLmFsbCwgdG9kb3MuY29tcGxldGVkQ291bnQsIHRvZG9zLm9wZW5Db3VudClcclxuXHQvLyBmaWx0ZXIgdGhlIGxpc3RcclxuXHRmaWx0ZXIodG9kb3MuYWxsLCB0b2Rvcy5saXN0LCBmaWx0ZXJOYW1lKVxyXG5cdC8vIGFsbG93IHRvIGNsZWFyIGFsbCBjb21wbGV0ZWQgdG9kb3NcclxuXHRjbGVhckNvbXBsZXRlZCh0b2Rvcy5hbGwsIHRvZG9zLmNsZWFyQ29tcGxldGVkKVxyXG5cdC8vIGxvYWQgYSB0b2RvIGxpc3QgYW5kIHNhdmUgY2hhbmdlc1xyXG5cdGxvY2FsKGxpc3QsIHRvZG9zLmFsbClcclxuXHQvLyBzZXQgdGhlIGxpc3QgbmFtZSBhbmQgYSBmaWx0ZXIgdG8gc3RhcnQgdGhlIGFwcFxyXG5cdHdpbmRvdy5vbmxvYWQgPSAoKSA9PiB7XHJcblx0XHRmaWx0ZXJOYW1lLmVtaXRTZXQoZG9jdW1lbnQubG9jYXRpb24uaGFzaC5zdWJzdHJpbmcoMikpXHJcblx0XHRsaXN0LmVtaXRTZXQoZG9jdW1lbnQubG9jYXRpb24uc2VhcmNoLnN1YnN0cigxKSB8fCBcInRvZG9zLWV2ZVwiKVxyXG5cdH1cclxuXHR3aW5kb3cub25oYXNoY2hhbmdlID0gKCkgPT4ge1xyXG5cdFx0ZmlsdGVyTmFtZS5lbWl0U2V0KGRvY3VtZW50LmxvY2F0aW9uLmhhc2guc3Vic3RyaW5nKDIpKVxyXG5cdH1cclxuXHJcbn0pKHdpbmRvdyk7XHJcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3RzL2FwcC50cyIsIlxyXG5pbXBvcnQge0NvbnRleHQsIFZhbHVlLCBOdW1iZXJWYWx1ZSwgQm9vbGVhblZhbHVlLCBMaXN0LCBDb21tYW5kfSBmcm9tICdldmUnXHJcblxyXG5leHBvcnQgY2xhc3MgVG9Eb3MgZXh0ZW5kcyBDb250ZXh0IHtcclxuXHRsaXN0ID0gbmV3IExpc3QoVG9EbylcclxuXHRhbGwgPSBuZXcgTGlzdChUb0RvKVxyXG5cdG9wZW5Db3VudCA9IG5ldyBOdW1iZXJWYWx1ZSgwKVxyXG5cdGNvbXBsZXRlZENvdW50ID0gbmV3IE51bWJlclZhbHVlKDApXHJcblx0Y2xlYXJDb21wbGV0ZWQgPSBuZXcgQ29tbWFuZCgpXHJcbn1cclxuXHJcbmV4cG9ydCBjbGFzcyBUb0RvIGV4dGVuZHMgQ29udGV4dCB7XHJcblx0dGV4dCA9IG5ldyBWYWx1ZTxzdHJpbmc+KClcclxuXHRjb21wbGV0ZWQgPSBuZXcgQm9vbGVhblZhbHVlKGZhbHNlKVxyXG59XHJcblxyXG5cclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vdHMvY29udGV4dC50cyIsIlxyXG5pbXBvcnQgeyBWaWV3YWJsZVZhbHVlLCBkb20sIG1hdGNoIH0gZnJvbSAnZXZlJ1xyXG5pbXBvcnQgeyBUb0RvLCBUb0RvcyB9IGZyb20gJy4uL2NvbnRleHQnXHJcbmltcG9ydCB7IHRvZG9JdGVtIH0gZnJvbSAnLi9Ub2RvSXRlbSdcclxuXHJcbmNvbnN0IHtFLCBwaWNrLCBlYWNoLCBrZXlDb2RlfSA9IGRvbVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIHRvZG9BcHAoY29udGFpbmVyOiBFbGVtZW50LCB0b2RvczogVG9Eb3MsIGZpbHRlcjogVmlld2FibGVWYWx1ZTxzdHJpbmc+KSB7XHJcblxyXG5cdGxldCBjaGtBbGw6IEhUTUxJbnB1dEVsZW1lbnRcclxuXHRjb25zdCBwYW5lbHMgPSBbXHJcblx0XHQvLyB0aGUgaGVhZGVyIHBhbmVsXHJcblx0XHRFLmhlYWRlcih7IGNsYXNzOiAnaGVhZGVyJyB9LFxyXG5cdFx0XHRFLmgxKCd0b2RvcycpLFxyXG5cdFx0XHRFLmlucHV0KHsgY2xhc3M6ICduZXctdG9kbycsIHBsYWNlaG9sZGVyOiAnV2hhdCBuZWVkcyB0byBiZSBkb25lPycsIGF1dG9mb2N1czogdHJ1ZSB9LFxyXG5cdFx0XHRcdHsgb25rZXl1cDogb25JbnB1dEtleSB9KSxcclxuXHRcdFx0RS5oZWFkZXIpLFxyXG5cdFx0Ly8gdGhlIG1haW4gcGFuZWxcclxuXHRcdEUuc2VjdGlvbihcclxuXHRcdFx0Ly8gb25lIG9mIG1hbnkgd2F5cyB0byBkeW5hbWljYWxseSBzaG93L2hpZGUgYW4gZWxlbWVudFxyXG5cdFx0XHR7IGNsYXNzOiB7IG1haW46IHRydWUsIGhpZGRlbjogbWF0Y2godG9kb3MuYWxsLmNvdW50LCAwKSB9IH0sXHJcblx0XHRcdGNoa0FsbCA9IEUuaW5wdXQoeyBjbGFzczogJ3RvZ2dsZS1hbGwnLCB0eXBlOiAnY2hlY2tib3gnIH0sXHJcblx0XHRcdFx0eyB0aXRsZTogJ01hcmsgYWxsIGFzIGNvbXBsZXRlJywgY2hlY2tlZDogbWF0Y2godG9kb3Mub3BlbkNvdW50LCAwKSB9KSxcclxuXHRcdFx0RS51bCh7IGNsYXNzOiAndG9kby1saXN0JyB9LCBlYWNoKHRvZG9zLmxpc3QsIHRvZG9JdGVtKSksXHJcblx0XHRcdEUuc2VjdGlvbiksXHJcblx0XHQvLyB0aGUgZm9vdGVyIHBhbmVsXHJcblx0XHRFLmZvb3Rlcih7IGNsYXNzOiAnZm9vdGVyJyB9LFxyXG5cdFx0XHQvLyBhbm90aGVyIHdheSB0byBkeW5hbWljYWxseSBzaG93L2hpZGUgYW4gZWxlbWVudFxyXG5cdFx0XHR7IGNsYXNzOiBwaWNrKHRvZG9zLmFsbC5jb3VudCwgeyAwOiAnaGlkZGVuJyB9KSB9LFxyXG5cdFx0XHRFLnNwYW4oeyBjbGFzczogJ3RvZG8tY291bnQnIH0sXHJcblx0XHRcdFx0RS5zdHJvbmcodG9kb3Mub3BlbkNvdW50KSwgJyBpdGVtJywgcGljayh0b2Rvcy5vcGVuQ291bnQsIHsgMTogJycgfSwgJ3MnKSwgJyBsZWZ0JyksXHJcblx0XHRcdEUudWwoeyBjbGFzczogJ2ZpbHRlcnMnIH0sXHJcblx0XHRcdFx0W1snJywgJ0FsbCddLCBbJ2FjdGl2ZScsICdBY3RpdmUnXSwgWydjb21wbGV0ZWQnLCAnQ29tcGxldGVkJ11dLm1hcCgodikgPT5cclxuXHRcdFx0XHRcdEUubGkoRS5hKHsgaHJlZjogJyMvJyArIHZbMF0sIGNsYXNzOiB7IHNlbGVjdGVkOiBtYXRjaChmaWx0ZXIsIHZbMF0pIH0gfSxcclxuXHRcdFx0XHRcdFx0dlsxXSkpKSxcclxuXHRcdFx0XHRFLnVsKSxcclxuXHRcdFx0Ly8geWV0IGFub3RoZXIgd2F5IHRvIGR5bmFtaWNhbGx5IHNob3cvaGlkZSAob3IgYmV0dGVyIGFkZC9yZW1vdmUgZnJvbSBET00pXHJcblx0XHRcdHBpY2sodG9kb3MuY29tcGxldGVkQ291bnQsIHsgMDogJycgfSxcclxuXHRcdFx0XHRFLmJ1dHRvbih7IGNsYXNzOiAnY2xlYXItY29tcGxldGVkJywgb25jbGljazogdG9kb3MuY2xlYXJDb21wbGV0ZWQgfSxcclxuXHRcdFx0XHRcdCdDbGVhciBjb21wbGV0ZWQnKSksXHJcblx0XHRcdEUuZm9vdGVyKVxyXG5cdF1cclxuXHJcblx0Ly8gaGFuZGxlIHVzZXIgZXZlbnRzXHJcblx0ZnVuY3Rpb24gb25JbnB1dEtleShldm4pIHtcclxuXHRcdGlmIChldm4ua2V5Q29kZSA9PT0ga2V5Q29kZS5FTlRFUikge1xyXG5cdFx0XHQvLyBhZGQgbmV3IHRvZG9cclxuXHRcdFx0Y29uc3QgdHh0ID0gdGhpcy52YWx1ZS50cmltKClcclxuXHRcdFx0aWYgKHR4dCkgdG9kb3MuYWxsLmVtaXRBZGQodG9kb3MuYWxsLmNyZWF0ZUl0ZW0oKSkudGV4dC5lbWl0U2V0KHR4dClcclxuXHRcdFx0dGhpcy52YWx1ZSA9ICcnXHJcblx0XHR9XHJcblx0fVxyXG5cdC8vIGNvbXBsZXRlIGFsbFxyXG5cdC8vIHRoaXMgY291bGQgYWxzbyBiZSBleHRyYWN0ZWQgYXMgYSBDb21tYW5kIChzZWUgYWxsSW5PbmUudHMpXHJcblx0Y2hrQWxsLm9uY2xpY2sgPSAoKSA9PiB7XHJcblx0XHRjb25zdCB2ID0gY2hrQWxsLmNoZWNrZWRcclxuXHRcdHRvZG9zLmFsbC5lYWNoKCh2YWwpID0+IHZhbC5jb21wbGV0ZWQuZW1pdFNldCh2KSlcclxuXHR9XHJcblxyXG5cdC8vIGluc2VydCBwYW5lbHMgaW50byBjb250YWluZXJcclxuXHRmb3IgKGxldCBwYW5lbCBvZiBwYW5lbHMpIGNvbnRhaW5lci5hcHBlbmRDaGlsZChwYW5lbClcclxufVxyXG5cclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vdHMvdWkvVG9kb0FwcC50cyIsIlxyXG5pbXBvcnQgeyBkb20gfSBmcm9tICdldmUnXHJcbmltcG9ydCB7IFRvRG8gfSBmcm9tICcuLi9jb250ZXh0J1xyXG5cclxuY29uc3Qge0UsIGtleUNvZGV9ID0gZG9tXHJcblxyXG5leHBvcnQgZnVuY3Rpb24gdG9kb0l0ZW0odG9kbzogVG9Ebykge1xyXG5cclxuXHQvLyBhIGxpc3QgaXRlbVxyXG5cdGxldCBlZGl0XHJcblx0Y29uc3QgZWwgPSBFLmxpKHsgY2xhc3M6IHsgY29tcGxldGVkOiB0b2RvLmNvbXBsZXRlZCB9IH0sXHJcblx0XHRFLmRpdih7IGNsYXNzOiAndmlldycgfSxcclxuXHRcdFx0RS5pbnB1dCh7IGNsYXNzOiAndG9nZ2xlJywgdHlwZTogJ2NoZWNrYm94JywgY2hlY2tlZDogdG9kby5jb21wbGV0ZWQgfSxcclxuXHRcdFx0XHR7IG9uY2hhbmdlOiB0b2RvLmNvbXBsZXRlZCB9KSxcclxuXHRcdFx0RS5sYWJlbCh0b2RvLnRleHQsIHsgb25kYmxjbGljazogb25FZGl0IH0pLFxyXG5cdFx0XHRFLmJ1dHRvbih7IGNsYXNzOiAnZGVzdHJveScsIG9uY2xpY2s6ICgpID0+IHRvZG8uZW1pdERlbGV0ZSgpIH0pLFxyXG5cdFx0XHRFLmRpdiksXHJcblx0XHRlZGl0ID0gRS5pbnB1dCh7IGNsYXNzOiAnZWRpdCcsIHZhbHVlOiB0b2RvLnRleHQsIG9uYmx1cjogb25BY2NlcHQsIG9ua2V5ZG93bjogb25LZXkgfSksXHJcblx0XHRFLmxpKVxyXG5cclxuXHQvLyBoYW5kbGUgdXNlciBldmVudHNcclxuXHRmdW5jdGlvbiBvbkVkaXQoKSB7XHJcblx0XHRlbC5jbGFzc0xpc3QuYWRkKCdlZGl0aW5nJylcclxuXHRcdGVkaXQuZm9jdXMoKVxyXG5cdH1cclxuXHRmdW5jdGlvbiBvbkFjY2VwdCgpIHtcclxuXHRcdGVsLmNsYXNzTGlzdC5yZW1vdmUoJ2VkaXRpbmcnKVxyXG5cdFx0Y29uc3QgdHh0ID0gZWRpdC52YWx1ZS50cmltKClcclxuXHRcdGlmICghdHh0KSB0b2RvLmVtaXREZWxldGUoKVxyXG5cdFx0ZWxzZSB0b2RvLnRleHQuZW1pdFNldCh0eHQpXHJcblx0fVxyXG5cdGZ1bmN0aW9uIG9uS2V5KGV2bikge1xyXG5cdFx0aWYgKGV2bi5rZXlDb2RlID09PSBrZXlDb2RlLkVOVEVSKSBlZGl0LmJsdXIoKVxyXG5cdFx0ZWxzZSBpZiAoZXZuLmtleUNvZGUgPT09IGtleUNvZGUuRVNDQVBFKSB7XHJcblx0XHRcdGVkaXQudmFsdWUgPSB0b2RvLnRleHQudmFsKClcclxuXHRcdFx0ZWRpdC5ibHVyKClcclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdHJldHVybiBlbFxyXG59XHJcblxyXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi90cy91aS9Ub2RvSXRlbS50cyIsIlxyXG5pbXBvcnQgeyBOdW1iZXJWYWx1ZSwgTGlzdCwgQ29tbWFuZCwgY291bnQgfSBmcm9tICdldmUnXHJcbmltcG9ydCB7IFRvRG8gfSBmcm9tICcuLi9jb250ZXh0J1xyXG5cclxuLy8gY29tcGxldGVkIGFuZCBvcGVuIHRvZG9zXHJcbmV4cG9ydCBmdW5jdGlvbiBjb3VudGluZyh0b2RvczogTGlzdDxUb0RvPixcclxuXHRjb21wbGV0ZWRDb3VudDogTnVtYmVyVmFsdWUsIG9wZW5Db3VudDogTnVtYmVyVmFsdWUpIHtcclxuXHJcblx0Ly8gdXNpbmcgY29ubmVjdG9ycy4uLlxyXG5cdC8vY291bnQodG9kb3MsIHsgY29tcGxldGVkOiB0cnVlIH0sIGNvbXBsZXRlZENvdW50KVxyXG5cdC8vY291bnQodG9kb3MsIHsgY29tcGxldGVkOiBmYWxzZSB9LCBvcGVuQ291bnQpXHJcblxyXG5cdC8vIG9yIHdpdGggZGlmZmVyZW50IGNvbm5lY3RvcnMuLi5cclxuXHQvL2NvdW50KHRvZG9zLCB7IGNvbXBsZXRlZDogdHJ1ZSB9LCBjb21wbGV0ZWRDb3VudClcclxuXHQvLyBUT0RPOiBpbXBsZW1lbnRcclxuXHQvL21pbnVzKGNvbXBsZXRlZENvdW50LCB0b2Rvcy5jb3VudCwgb3BlbkNvdW50KVxyXG5cclxuXHQvLyBvciBkaXJlY3RseSBjb3VudGluZyBib3RoXHJcblx0Y29uc3QgY291bnRGbiA9ICgpID0+IHtcclxuXHRcdGxldCBjb21wbGV0ZWQgPSAwLCBvcGVuID0gMFxyXG5cdFx0dG9kb3MuZWFjaCgodG9kbykgPT4gdG9kby5jb21wbGV0ZWQudmFsKCkgPyBjb21wbGV0ZWQrKyA6IG9wZW4rKylcclxuXHRcdGNvbXBsZXRlZENvdW50LmVtaXRTZXQoY29tcGxldGVkKVxyXG5cdFx0b3BlbkNvdW50LmVtaXRTZXQob3BlbilcclxuXHR9XHJcblx0dG9kb3Mub25BZGQoKHRvZG86IFRvRG8pID0+IHRvZG8uY29tcGxldGVkLm9uU2V0KGNvdW50Rm4sIHRvZG9zKSlcclxuXHR0b2Rvcy5vblJlbW92ZShjb3VudEZuKVxyXG59XHJcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3RzL2xvZ2ljL2NvdW50LnRzIiwiXHJcbmltcG9ydCB7IFZhbHVlLCBMaXN0LCBtYXAsIGZpbHRlciBhcyBmbHRyIH0gZnJvbSAnZXZlJ1xyXG5pbXBvcnQgeyBUb0RvIH0gZnJvbSAnLi4vY29udGV4dCdcclxuXHJcbi8vIGZpbHRlciB0aGUgbGlzdCBvZiB0b2Rvc1xyXG5leHBvcnQgZnVuY3Rpb24gZmlsdGVyKGFsbDogTGlzdDxUb0RvPiwgZmlsdGVyZWQ6IExpc3Q8VG9Ebz4sIGZpbHRlcjogVmFsdWU8c3RyaW5nPikge1xyXG5cdGZsdHIoYWxsLCB7IGNvbXBsZXRlZDogbWFwKGZpbHRlciwgeyBjb21wbGV0ZWQ6IHRydWUsIGFjdGl2ZTogZmFsc2UsICcnOiB2b2lkIDAgfSkgfSwgZmlsdGVyZWQpXHJcbn1cclxuXHJcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3RzL2xvZ2ljL2ZpbHRlci50cyIsIlxyXG5pbXBvcnQgeyBMaXN0LCBDb21tYW5kIH0gZnJvbSAnZXZlJ1xyXG5pbXBvcnQgeyBUb0RvIH0gZnJvbSAnLi4vY29udGV4dCdcclxuXHJcbi8vIGFsbG93IHRvIGNsZWFyIGFsbCBjb21wbGV0ZWQgdG9kb3NcclxuZXhwb3J0IGZ1bmN0aW9uIGNsZWFyQ29tcGxldGVkKGFsbDogTGlzdDxUb0RvPiwgY21kOiBDb21tYW5kKSB7XHJcblx0Y21kLm9uUGVyZm9ybSgoKSA9PiB7XHJcblx0XHRhbGwuZWFjaCgodG9kbykgPT4gdG9kby5jb21wbGV0ZWQudmFsKCkgJiYgdG9kby5lbWl0RGVsZXRlKCkpXHJcblx0fSlcclxufVxyXG5cclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vdHMvbG9naWMvY2xlYXJDb21wbGV0ZWQudHMiLCJcclxuaW1wb3J0IHtWYWx1ZSwgTGlzdCwgQnJvd3NlckRvY1N0b3JlLCBqc29ufSBmcm9tICdldmUnXHJcbmltcG9ydCB7VG9Eb30gZnJvbSAnLi4vY29udGV4dCdcclxuXHJcbmNvbnN0IHtlYWNoLCBpZE1hcH0gPSBqc29uXHJcblxyXG4vLyBzdG9yZSBpbiBsb2NhbCBzdG9yYWdlXHJcbmV4cG9ydCBmdW5jdGlvbiBsb2NhbChsaXN0TmFtZTogVmFsdWU8c3RyaW5nPiwgdG9kb3M6IExpc3Q8VG9Ebz4pIHtcclxuXHJcblx0Y29uc3QgdG1wbCA9IHtcclxuXHRcdHRvZG9zOiBlYWNoKHRvZG9zLCAodG9kbykgPT4gKHtcclxuXHRcdFx0aWQ6IGlkTWFwKHRvZG8pLFxyXG5cdFx0XHR0ZXh0OiB0b2RvLnRleHQsXHJcblx0XHRcdGNvbXBsZXRlZDogdG9kby5jb21wbGV0ZWRcclxuXHRcdH0pKVxyXG5cdH1cclxuXHJcblx0Y29uc3Qgc3RvcmUgPSBuZXcgQnJvd3NlckRvY1N0b3JlKGxvY2FsU3RvcmFnZSlcclxuXHJcblx0Ly8gbG9hZCB0b2RvcyBhcyBzb29uIGFzIHRoZSBsaXN0IG5hbWUgaXMgc2V0XHJcblx0bGlzdE5hbWUub25TZXQoKHZhbCkgPT4ge1xyXG5cdFx0c3RvcmUubG9hZCh2YWwpLnRoZW4oKGQpID0+IHtcclxuXHRcdFx0bGV0IGlkR2VuID0gZCAmJiBkLmlkR2VuIHx8IDBcclxuXHRcdFx0Y29uc3QgaWRzID0gbmV3IGpzb24uSWRDb250ZXh0TWFwKCgpID0+IGlkR2VuKyspXHJcblx0XHRcdGpzb24uYnVpbGQodG1wbCwgaWRzLCAoZGF0YSkgPT4ge1xyXG5cdFx0XHRcdGRhdGEuaWRHZW4gPSBpZEdlblxyXG5cdFx0XHRcdHN0b3JlLnNhdmUodmFsLCBkYXRhKVxyXG5cdFx0XHR9KVxyXG5cdFx0XHRqc29uLmVtaXQodG1wbCwgZCwgaWRzKVxyXG5cdFx0fSlcclxuXHR9KVxyXG5cclxufVxyXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi90cy9zdG9yYWdlL2xvY2FsLnRzIl0sInNvdXJjZVJvb3QiOiIifQ==