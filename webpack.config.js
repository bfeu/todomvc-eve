var CommonsChunkPlugin = require("webpack/lib/optimize/CommonsChunkPlugin");

module.exports = {
	entry: {
		app: './ts/app.ts',
		allInOne: './ts/allInOne.ts'
	},
	output: {
		path: __dirname + '/.',
		filename: '[name].js'
	},
	plugins: [
		new CommonsChunkPlugin("eve.js")
	],
	resolve: {
		extensions: ['', '.ts', '.tsx', '.js'],
	},
	module: {
		loaders: [
			{ test: /\.ts$/, loader: 'ts-loader' },
			{ test: /\.json$/, loader: 'json' }
		]
	},
	devtool: '#inline-source-map',
	node: {
		global: false, process: false, __dirname: false, __filename: false, Buffer: false
	},
	externals: [
		'buffer', 'crypto'
	]
}
