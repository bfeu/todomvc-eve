
import { List, Command } from 'eve'
import { ToDo } from '../context'

// allow to clear all completed todos
export function clearCompleted(all: List<ToDo>, cmd: Command) {
	cmd.onPerform(() => {
		all.each((todo) => todo.completed.val() && todo.emitDelete())
	})
}

