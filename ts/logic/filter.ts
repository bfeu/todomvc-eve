
import { Value, List, map, filter as fltr } from 'eve'
import { ToDo } from '../context'

// filter the list of todos
export function filter(all: List<ToDo>, filtered: List<ToDo>, filter: Value<string>) {
	fltr(all, { completed: map(filter, { completed: true, active: false, '': void 0 }) }, filtered)
}

