
import { NumberValue, List, Command, count } from 'eve'
import { ToDo } from '../context'

// completed and open todos
export function counting(todos: List<ToDo>,
	completedCount: NumberValue, openCount: NumberValue) {

	// using connectors...
	//count(todos, { completed: true }, completedCount)
	//count(todos, { completed: false }, openCount)

	// or with different connectors...
	//count(todos, { completed: true }, completedCount)
	// TODO: implement
	//minus(completedCount, todos.count, openCount)

	// or directly counting both
	const countFn = () => {
		let completed = 0, open = 0
		todos.each((todo) => todo.completed.val() ? completed++ : open++)
		completedCount.emitSet(completed)
		openCount.emitSet(open)
	}
	todos.onAdd((todo: ToDo) => todo.completed.onSet(countFn, todos))
	todos.onRemove(countFn)
}
