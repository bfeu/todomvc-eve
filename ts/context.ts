
import {Context, Value, NumberValue, BooleanValue, List, Command} from 'eve'

export class ToDos extends Context {
	list = new List(ToDo)
	all = new List(ToDo)
	openCount = new NumberValue(0)
	completedCount = new NumberValue(0)
	clearCompleted = new Command()
}

export class ToDo extends Context {
	text = new Value<string>()
	completed = new BooleanValue(false)
}


