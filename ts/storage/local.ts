
import {Value, List, BrowserDocStore, json} from 'eve'
import {ToDo} from '../context'

const {each, idMap} = json

// store in local storage
export function local(listName: Value<string>, todos: List<ToDo>) {

	const tmpl = {
		todos: each(todos, (todo) => ({
			id: idMap(todo),
			text: todo.text,
			completed: todo.completed
		}))
	}

	const store = new BrowserDocStore(localStorage)

	// load todos as soon as the list name is set
	listName.onSet((val) => {
		store.load(val).then((d) => {
			let idGen = d && d.idGen || 0
			const ids = new json.IdContextMap(() => idGen++)
			json.build(tmpl, ids, (data) => {
				data.idGen = idGen
				store.save(val, data)
			})
			json.emit(tmpl, d, ids)
		})
	})

}
