
import { ViewableValue, dom, match } from 'eve'
import { ToDo, ToDos } from '../context'
import { todoItem } from './TodoItem'

const {E, pick, each, keyCode} = dom

export function todoApp(container: Element, todos: ToDos, filter: ViewableValue<string>) {

	let chkAll: HTMLInputElement
	const panels = [
		// the header panel
		E.header({ class: 'header' },
			E.h1('todos'),
			E.input({ class: 'new-todo', placeholder: 'What needs to be done?', autofocus: true },
				{ onkeyup: onInputKey }),
			E.header),
		// the main panel
		E.section(
			// one of many ways to dynamically show/hide an element
			{ class: { main: true, hidden: match(todos.all.count, 0) } },
			chkAll = E.input({ class: 'toggle-all', type: 'checkbox' },
				{ title: 'Mark all as complete', checked: match(todos.openCount, 0) }),
			E.ul({ class: 'todo-list' }, each(todos.list, todoItem)),
			E.section),
		// the footer panel
		E.footer({ class: 'footer' },
			// another way to dynamically show/hide an element
			{ class: pick(todos.all.count, { 0: 'hidden' }) },
			E.span({ class: 'todo-count' },
				E.strong(todos.openCount), ' item', pick(todos.openCount, { 1: '' }, 's'), ' left'),
			E.ul({ class: 'filters' },
				[['', 'All'], ['active', 'Active'], ['completed', 'Completed']].map((v) =>
					E.li(E.a({ href: '#/' + v[0], class: { selected: match(filter, v[0]) } },
						v[1]))),
				E.ul),
			// yet another way to dynamically show/hide (or better add/remove from DOM)
			pick(todos.completedCount, { 0: '' },
				E.button({ class: 'clear-completed', onclick: todos.clearCompleted },
					'Clear completed')),
			E.footer)
	]

	// handle user events
	function onInputKey(evn) {
		if (evn.keyCode === keyCode.ENTER) {
			// add new todo
			const txt = this.value.trim()
			if (txt) todos.all.emitAdd(todos.all.createItem()).text.emitSet(txt)
			this.value = ''
		}
	}
	// complete all
	// this could also be extracted as a Command (see allInOne.ts)
	chkAll.onclick = () => {
		const v = chkAll.checked
		todos.all.each((val) => val.completed.emitSet(v))
	}

	// insert panels into container
	for (let panel of panels) container.appendChild(panel)
}

