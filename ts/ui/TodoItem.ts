
import { dom } from 'eve'
import { ToDo } from '../context'

const {E, keyCode} = dom

export function todoItem(todo: ToDo) {

	// a list item
	let edit
	const el = E.li({ class: { completed: todo.completed } },
		E.div({ class: 'view' },
			E.input({ class: 'toggle', type: 'checkbox', checked: todo.completed },
				{ onchange: todo.completed }),
			E.label(todo.text, { ondblclick: onEdit }),
			E.button({ class: 'destroy', onclick: () => todo.emitDelete() }),
			E.div),
		edit = E.input({ class: 'edit', value: todo.text, onblur: onAccept, onkeydown: onKey }),
		E.li)

	// handle user events
	function onEdit() {
		el.classList.add('editing')
		edit.focus()
	}
	function onAccept() {
		el.classList.remove('editing')
		const txt = edit.value.trim()
		if (!txt) todo.emitDelete()
		else todo.text.emitSet(txt)
	}
	function onKey(evn) {
		if (evn.keyCode === keyCode.ENTER) edit.blur()
		else if (evn.keyCode === keyCode.ESCAPE) {
			edit.value = todo.text.val()
			edit.blur()
		}
	}

	return el
}

