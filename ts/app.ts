
import { Value } from 'eve'
import { ToDos } from './context'
import { todoApp } from './ui/TodoApp'
import { counting } from './logic/count'
import { filter } from './logic/filter'
import { clearCompleted } from './logic/clearCompleted'
import { local } from './storage/local'

(function(window) {
	'use strict';

	// list name
	const list = new Value<string>().contextName("list name")
	// the list of todos
	const todos = new ToDos().contextName("todos")
	// the filter name
	const filterName = new Value<string>().contextName("filter name")

	// render the Web user interface
	todoApp(document.getElementsByClassName("todoapp")[0], todos, filterName)
	// count the completed and open todos
	counting(todos.all, todos.completedCount, todos.openCount)
	// filter the list
	filter(todos.all, todos.list, filterName)
	// allow to clear all completed todos
	clearCompleted(todos.all, todos.clearCompleted)
	// load a todo list and save changes
	local(list, todos.all)
	// set the list name and a filter to start the app
	window.onload = () => {
		filterName.emitSet(document.location.hash.substring(2))
		list.emitSet(document.location.search.substr(1) || "todos-eve")
	}
	window.onhashchange = () => {
		filterName.emitSet(document.location.hash.substring(2))
	}

})(window);
