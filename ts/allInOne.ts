import {
	Context, Value, NumberValue, BooleanValue, List, Command,
	count, filter, map, match,
	BrowserDocStore, json,
	dom
} from 'eve'

/*
All-in-one version of the complete todomvc application. This shows all the
pieces of code in one view. It allows to simpler identify all the aspects
without the module overhead.
See app.ts for the same application split into modules to be maintained
separately.
*/

// --- model and build contexts
class ToDos extends Context {
	list = new List(ToDo)
	all = new List(ToDo)
	openCount = new NumberValue(0)
	completedCount = new NumberValue(0)
	clearCompleted = new Command()
	toggleCompleteAll = new Command()
}

class ToDo extends Context {
	text = new Value<string>()
	completed = new BooleanValue(false)
}

const list = new Value<string>().contextName("list name")
const todos = new ToDos().contextName("todos")
const filterName = new Value<string>().contextName("filter name")


// --- define the application logic
// count the completed and open todos
count(todos.all, { completed: true }, todos.completedCount)
count(todos.all, { completed: false }, todos.openCount)
// filter the list
filter(todos.all, { completed: map(filterName, { completed: true, active: false, '': void 0 }) },
	todos.list)
// allow to clear all completed todos
todos.clearCompleted.onPerform(() => {
	todos.all.each(todo => todo.completed.val() && todo.emitDelete())
})
// allow to complete or open all open todos
todos.toggleCompleteAll.onPerform(() => {
	todos.all.each(todo => todo.completed.emitSet(todos.openCount.val() !== 0))
})


// --- store as JSON in Browser localStorage
const tmpl = {
	todos: json.each(todos.all, todo => ({
		id: json.idMap(todo),
		text: todo.text,
		completed: todo.completed
	}))
}
const store = new BrowserDocStore(localStorage)

// load todos as soon as the list name is set
list.onSet(val => {
	store.load(val).then(d => {
		let idGen = d && d.idGen || 0
		const ids = new json.IdContextMap(() => idGen++)
		json.build(tmpl, ids, data => {
			data.idGen = idGen
			store.save(val, data)
		})
		json.emit(tmpl, d, ids)
	})
})


// --- render the application
const {E, pick, each, keyCode} = dom
const panels = [
	// the header panel
	E.header({ class: 'header' },
		E.h1('todos'),
		E.input({ class: 'new-todo', placeholder: 'What needs to be done?', autofocus: true },
			{ onkeyup: onInputKey }),
		E.header),
	// the main panel
	E.section({ class: { main: true, hidden: match(todos.all.count, 0) } },
		E.input({ class: 'toggle-all', type: 'checkbox', title: 'Mark all as complete' },
			{ checked: match(todos.openCount, 0), onclick: todos.toggleCompleteAll }),
		E.ul({ class: 'todo-list' }, each(todos.list, todoItem)),
		E.section),
	// the footer panel
	E.footer({ class: 'footer' }, { class: pick(todos.all.count, { 0: 'hidden' }) },
		E.span({ class: 'todo-count' },
			E.strong(todos.openCount), ' item', pick(todos.openCount, { 1: '' }, 's'), ' left'),
		E.ul({ class: 'filters' },
			[['', 'All'], ['active', 'Active'], ['completed', 'Completed']].map((v) =>
				E.li(E.a({ href: '#/' + v[0], class: { selected: match(filterName, v[0]) } },
					v[1]))),
			E.ul),
		pick(todos.completedCount, { 0: '' },
			E.button({ class: 'clear-completed', onclick: todos.clearCompleted },
				'Clear completed')),
		E.footer)
]

// handle user events
function onInputKey(evn) {
	if (evn.keyCode === keyCode.ENTER) {
		// add new todo
		const txt = this.value.trim()
		if (txt) todos.all.emitAdd(todos.all.createItem()).text.emitSet(txt)
		this.value = ''
	}
}

// the todo item component
function todoItem(todo: ToDo) {

	// a list item
	let edit
	const el = E.li({ class: { completed: todo.completed } },
		E.div({ class: 'view' },
			E.input({ class: 'toggle', type: 'checkbox', checked: todo.completed },
				{ onchange: todo.completed }),
			E.label(todo.text, { ondblclick: onEdit }),
			E.button({ class: 'destroy', onclick: () => todo.emitDelete() }),
			E.div),
		edit = E.input({ class: 'edit', value: todo.text, onblur: onOk, onkeydown: onKey }),
		E.li)

	// handle user events
	function onEdit() {
		el.classList.add('editing')
		edit.focus()
	}
	function onOk() {
		el.classList.remove('editing')
		const txt = edit.value.trim()
		if (!txt) todo.emitDelete()
		else todo.text.emitSet(txt)
	}
	function onKey(evn) {
		if (evn.keyCode === keyCode.ENTER) edit.blur()
		else if (evn.keyCode === keyCode.ESCAPE) {
			edit.value = todo.text.val()
			edit.blur()
		}
	}

	return el
}


// -- build 
(function(window) {
	'use strict';

	const container = document.getElementsByClassName("todoapp")[0]
	for (let panel of panels) container.appendChild(panel)

	// set the list name and a filter to start the app
	window.onload = () => {
		filterName.emitSet(document.location.hash.substring(2))
		list.emitSet(document.location.search.substr(1) || "todos-eve")
	}
	window.onhashchange = () => {
		filterName.emitSet(document.location.hash.substring(2))
	}

})(window);
